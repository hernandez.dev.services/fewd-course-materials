/**
 * Challenge 1 - Click Counter
 *
 * Display the number of times the button has been clicked.
 * The button has an ID 'click-target' and the area to
 * display the count has the ID 'click-count'.
 *
 * Use the 'innerText' property in HTML elements to set
 * the text content inside the element. Ex:
 *
 * myHeader.innerText = 'Hello, World!';
 */
document.addEventListener("DOMContentLoaded", function() {
  // Write the solution in here...
});
