document.addEventListener("DOMContentLoaded", function() {
  const expandingToggle = document.querySelector("#expanding-toggle");

  const expandingPanel = document.querySelector("#expanding-panel");

  expandingToggle.addEventListener("click", function() {
    if (expandingPanel.classList.contains("expand")) {
      expandingPanel.classList.replace("expand", "collapse");
    } else {
      expandingPanel.classList.replace("collapse", "expand");
    }
  });
});
