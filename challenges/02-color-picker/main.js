/**
 * Challenge 2 - Color Picker
 *
 * Display the selected color in the centered display.
 * The red button has the ID "color-option-red", the
 * blue button has the ID "color-option-blue" and the
 * yellow button has the ID "color-option-yellow". The
 * centered display has the ID "color-display".
 *
 * Use the 'classList' property in the HTML elements to
 * change the CSS classes on an element.
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/Element/classList
 */
document.addEventListener("DOMContentLoaded", function() {
  // Write the solution in here...
});
