/**
 * Challenge 2 - Color Picker
 *
 * Display the selected color in the centered display.
 * The red button has the ID "color-option-red", the
 * blue button has the ID "color-option-blue" and the
 * yellow button has the ID "color-option-yellow". The
 * centered display has the ID "color-display".
 *
 * Use the 'classList' property in the HTML elements to
 * change the CSS classes on an element.
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/Element/classList
 */
document.addEventListener("DOMContentLoaded", function() {
  const colorDisplay = document.querySelector("#color-display");

  const redButton = document.querySelector("#color-option-red");

  const blueButton = document.querySelector("#color-option-blue");

  const yellowButton = document.querySelector("#color-option-yellow");

  redButton.addEventListener("click", function() {
    if (colorDisplay.classList.contains("blue")) {
      colorDisplay.classList.remove("blue");
    }
    if (colorDisplay.classList.contains("yellow")) {
      colorDisplay.classList.remove("yellow");
    }
    colorDisplay.classList.add("red");
  });

  blueButton.addEventListener("click", function() {
    if (colorDisplay.classList.contains("red")) {
      colorDisplay.classList.remove("red");
    }
    if (colorDisplay.classList.contains("yellow")) {
      colorDisplay.classList.remove("yellow");
    }
    colorDisplay.classList.add("blue");
  });

  yellowButton.addEventListener("click", function() {
    if (colorDisplay.classList.contains("red")) {
      colorDisplay.classList.remove("red");
    }
    if (colorDisplay.classList.contains("blue")) {
      colorDisplay.classList.remove("blue");
    }
    colorDisplay.classList.add("yellow");
  });
});
